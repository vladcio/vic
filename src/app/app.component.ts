import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from './services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  user: any;
  anon: boolean;
  loading = true;
  title = 'Vic Design';
  error = null;
  processing = false;
  loggedInUser: any;


  constructor(
    private authService: AuthService,
    private router: Router,
  ) {}

  ngOnInit(): void {

    // check use
    this.authService.userChange$.subscribe((user) => {
      this.loading = false;
      this.user = user;
      this.anon = !user;
    });
    this.loggedInUser = this.authService.getUser();

    this.authService.me();
  }

  submitLogout(form) {
    this.error = '';
     if (form.valid) {
      this.processing = true;
        this.authService.logout()
          .then((result) => {
          this.router.navigate(['/']);
          })
          .catch((err) => {
            this.error = err.error.error; // :-)
            this.processing = false;
          });
    }
  }

}
