import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl + '/pictures';

@Injectable()
export class PicturesService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getPictures(): any {
    return this.httpClient.get(`${API_URL}/pictures`)
      .toPromise();
  }

  getCategory(data): Promise<any> {
    return this.httpClient.post(`${API_URL}/find-category`, data)
    .toPromise();
  }


  addPicture(picture: any): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.httpClient.post(`${API_URL}/new-picture`, picture, options)
      .toPromise();
  }
}
