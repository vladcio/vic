import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl + '/categories';

@Injectable()
export class CategoriesService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getCategories(): any {
    return this.httpClient.get(`${API_URL}/categories-items`)
      .toPromise();
  }

  getCategory(data): Promise<any> {
    return this.httpClient.post(`${API_URL}/findcat`, data)
    .toPromise();
  }


  addItem(item: any): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.httpClient.post(`${API_URL}/new-item`, item, options)
      .toPromise();
  }

  addPhotoItem(formdata: any): Promise<any> {
    const options = {
      withCredentials: true,
    };
    return this.httpClient.post(`${API_URL}/item-photo`, formdata, options)
      .toPromise();
  }
}
