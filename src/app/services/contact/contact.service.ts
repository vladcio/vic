import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl + '/users';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(
    private httpClient: HttpClient,
    private router: Router,
  ) { }

  // sent Contact Form
  sendContactForm(data: any): Promise<any> {

    return this.httpClient.post(`${API_URL}/send-contact-form`, data)
      .toPromise()
      .then((res: Response) => res.json());
  }

  // sent Contact Form
  sendOfferForm(data: any): Promise<any> {
    return this.httpClient.post(`${API_URL}/send-offer-form`, data)
      .toPromise();
  }

    // sent Contact Form
    sendEmail(data: any): Promise<any> {
      return this.httpClient.post(`${API_URL}/send-email`, data)
        .toPromise();
    }
}
