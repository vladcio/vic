import { Component, OnInit, Input } from '@angular/core';
import { CategoriesService } from '../../services/categories/categories.service';

@Component({
  selector: 'app-models-page',
  templateUrl: '../../components/categories/model-category/model-category.component.html',
  styleUrls: ['../../components/categories/model-category/model-category.component.css']
})
export class ModelsPageComponent implements OnInit {

  data: any;
  items: Array<any>;
  categoryType = '';


  constructor(
    private categories: CategoriesService,
  ) { }

  ngOnInit() {

    this.data = {
      categoryType: this.categoryType
    };
    this.categories.getCategory(this.data)
      .then((items) => {
      this.items = items;
      this.showPage();
    });
  }

  showPage() {
    document.getElementById('loading').style.display = 'none';
    document.getElementById('list-all-items').style.display = 'block';
  }

  goBack () {
    window.history.back();
  }

}
