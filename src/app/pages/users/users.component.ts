import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  loggedInUser: any;
  anon: boolean;
  loading = true;
  error = null;

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
     // check use
    this.authService.me();

    // get logged in user
    this.loggedInUser = this.authService.getUser();
  }

}
