import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.css']
})
export class AboutPageComponent implements OnInit {
  cellon = '#cellon';
  mdf = '#mdf';
  formboard = '#formboard';
  plywood = '#plywood';

  constructor() { }

  ngOnInit() {
  }

  goBack () {
    window.history.back();
  }

  goTo(destination) {
    document.querySelector(destination).scrollIntoView({
      behavior: 'smooth'
    });
  }
}
