import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VICDHomeComponent } from './vicd-home.component';

describe('VICDHomeComponent', () => {
  let component: VICDHomeComponent;
  let fixture: ComponentFixture<VICDHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VICDHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VICDHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
