import { Component, OnInit } from '@angular/core';
import { PicturesService } from '../../services/pictures/pictures.service';

@Component({
  selector: 'app-gallery-page',
  templateUrl: '../../components/gallery/model-picture-category/model-picture-category.component.html',
  styleUrls: ['../../components/gallery/model-picture-category/model-picture-category.component.css']
})
export class GalleryPageComponent implements OnInit {

  data: any;
  pictures: Array<any>;
  categoryType = '';

  constructor(
    private picturesService: PicturesService,
  ) { }

  ngOnInit() {

    this.data = {
      categoryType: this.categoryType
    };
    this.picturesService.getCategory(this.data)
      .then((pictures) => {
      this.pictures = pictures;
      this.showPage();
    });
  }

  showPage() {
    document.getElementById('loading').style.display = 'none';
    document.getElementById('list-all-pictures').style.display = 'block';
  }

  goBack () {
    window.history.back();
  }
}
