import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pages-menu',
  templateUrl: './pages-menu.component.html',
  styleUrls: ['./pages-menu.component.css']
})

export class PagesMenuComponent implements OnInit {

  @Input() user: any;

  isModelsPage: Boolean = false;
  isPicturesPage: Boolean = false;
  isAdminPage: Boolean = false;

  constructor() { }

  ngOnInit() {

  }

  togglePages(type) {
    switch (type) {
      case 'models':
        this.isModelsPage = true;
        this.isPicturesPage = false;
        this.isAdminPage = false;
        break;
      case 'pictures':
        this.isPicturesPage = true;
        this.isModelsPage = false;
        this.isAdminPage = false;
        break;
      case 'admin':
        this.isPicturesPage = false;
        this.isModelsPage = false;
        this.isAdminPage = true;
        break;
      default:
        this.isModelsPage = false;
        this.isPicturesPage = false;
        this.isAdminPage = false;
        break;
    }
  }

}
