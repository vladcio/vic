import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-model-picture-category',
  templateUrl: './model-picture-category.component.html',
  styleUrls: ['./model-picture-category.component.css']
})
export class ModelPictureCategoryComponent implements OnInit {

  @Input() pictures: any;

    constructor() { }

    ngOnInit() {
      const loader = setInterval(() => {
        if (this.pictures) {
          this.showPage();
          clearInterval(loader);
        }
      });
    }

    showPage() {
      document.getElementById('loading').style.display = 'none';
      document.getElementById('list-all-pictures').style.display = 'block';
    }

    goBack () {
      window.history.back();
    }

}
