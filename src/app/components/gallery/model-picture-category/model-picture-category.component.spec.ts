import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelPictureCategoryComponent } from './model-picture-category.component';

describe('ModelPictureCategoryComponent', () => {
  let component: ModelPictureCategoryComponent;
  let fixture: ComponentFixture<ModelPictureCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelPictureCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelPictureCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
