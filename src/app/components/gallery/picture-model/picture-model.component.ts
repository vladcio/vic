import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';

const API_URL = environment.apiUrl;

@Component({
  selector: 'app-picture-model',
  templateUrl: './picture-model.component.html',
  styleUrls: ['./picture-model.component.css']
})
export class PictureModelComponent implements OnInit {

  @Input() picture: any;
  pictureLink: string;

  constructor() { }

  ngOnInit() {
    // establish picture pictureLink after upload
    this.pictureLink = API_URL + this.picture.picPath;
  }

}
