import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { AgGridNg2 } from 'ag-grid-angular';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridNg2;

  private gridApi;
  private gridColumnApi;

  title = 'app';
  showStatus;

  columnDefs = [
    {headerName: 'Unreaded', field: 'unreaded' },
    {headerName: 'Status', field: 'requestStatus', editable: true },
    {headerName: 'Name', field: 'name', editable: true },
    {headerName: 'Phone Number', field: 'phone', editable: true },
    {headerName: 'E-mail', field: 'email', editable: true },
    {headerName: 'Date', field: 'createdAt', filter: 'agDateColumnFilter' },
    {headerName: 'Message', field: 'message', editable: true },
  ];

  gridOptions = {
    columnDefs: this.columnDefs,
    rowData: null,
    enableColResize: true,
    onColumnResized: function(params) {
      console.log(params);
    }
  };

  loggedInUser: any;
  anon: boolean;
  loading = true;
  error = null;
  message: any;
  rowData: any;
  selectedNodes: any;

  constructor(
    private authService: AuthService,
    private httpClient: HttpClient,
  ) { }

  ngOnInit(): void {
     // check use
    this.authService.me();
    this.loggedInUser = this.authService.getUser();

    // get logged in user
    const loader = setInterval(() => {
      if (this.loggedInUser) {
        this.showPage();
        this.getInitialData();
        clearInterval(loader);
      }
    });
  }

  showPage() {
    document.getElementById('loading').style.display = 'none';
    document.getElementById('offers-container').style.display = 'block';
  }

  getInitialData() {
    this.rowData = this.loggedInUser.receivedMessage;
  }

  markAsReaded() {
    this.selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = this.selectedNodes.map( node => node.data );
    const data = {
      userId: this.loggedInUser._id,
      messageId: selectedData[0]._id,
    };
    this.authService.markAsReadMessage(data)
      .then((res) => {
        this.loggedInUser = res;
        this.rowData = this.loggedInUser.receivedMessage;
        this.agGrid.api.refreshCells();
      });
  }

  editRow() {
    this.selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = this.selectedNodes.map( node => node.data );
    const data = {
      userId: this.loggedInUser._id,
      messageId: selectedData[0]._id,
      messageEdited: {
        name: selectedData[0].name,
        phone: selectedData[0].phone,
        email: selectedData[0].email,
        messageContent: selectedData[0].message,
        requestStatus: selectedData[0].requestStatus,
      }
    };
    this.authService.editMessage(data)
      .then((res) => {
        this.loggedInUser = res;
        this.rowData = this.loggedInUser.receivedMessage;
        this.agGrid.api.refreshCells();
      });
  }

  sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }


  autoSizeAll() {
    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.rowData = this.loggedInUser.receivedMessage;
    this.autoSizeAll();
    this.gridApi.sizeColumnsToFit();
  }

  goBack () {
    window.history.back();
  }
}
