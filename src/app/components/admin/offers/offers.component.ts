import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { AgGridNg2 } from 'ag-grid-angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridNg2;

  private gridApi;
  private gridColumnApi;

  title = 'app';
  showStatus;

  columnDefs = [
    {headerName: 'Unreaded', field: 'unreaded' },
    {headerName: 'Id', field: 'offerId', suppressResize: true },
    {headerName: 'Request Status', field: 'requestStatus', editable: true },
    {headerName: 'Firstname', field: 'firstName', editable: true },
    {headerName: 'Surname', field: 'surName', editable: true },
    {headerName: 'Company', field: 'companyName', hide: true, editable: true },
    {headerName: 'Phone Number', field: 'phone', editable: true },
    {headerName: 'E-mail', field: 'email', editable: true },
    {headerName: 'Adreess', field: 'address', hide: true, editable: true },
    {headerName: 'City', field: 'city', editable: true, hide: true, },
    {headerName: 'Zipcode', field: 'zipCode', hide: true, editable: true },
    {headerName: 'Country', field: 'country', editable: true, hide: true, },
    {headerName: 'Profession', field: 'profession', hide: true, editable: true },
    {headerName: 'Other Profession', field: 'otherProfession', hide: true, editable: true },
    {headerName: 'Application', field: 'application', editable: true },
    {headerName: 'Other Application', field: 'otherApplication', hide: true, editable: true },
    {headerName: 'Category Type', field: 'categoryType', editable: true },
    {headerName: 'Model Number', field: 'modelId', editable: true },
    {headerName: 'Width', field: 'width', editable: true },
    {headerName: 'Height', field: 'height', editable: true },
    {headerName: 'Project Name', field: 'projectName', hide: true, editable: true },
    {headerName: 'Date', field: 'createdAt', filter: 'agDateColumnFilter', comparator: this.dateComparator },
    {headerName: 'Message Content', field: 'messageContent', editable: true },
    {headerName: 'Internal Comments', field: 'internalComments', editable: true },

  ];

  gridOptions = {
    columnDefs: this.columnDefs,
    rowData: null,
    enableColResize: true,
    onColumnResized: function(params) {
      console.log(params);
    }
  };

  loggedInUser: any;
  anon: boolean;
  loading = true;
  error = null;
  message: any;
  rowData: any;
  selectedNodes: any;

  constructor(
    private authService: AuthService,
    private httpClient: HttpClient,
  ) { }

  ngOnInit(): void {
     // check use
    this.authService.me();
    this.loggedInUser = this.authService.getUser();

    // get logged in user
    const loader = setInterval(() => {
      if (this.loggedInUser) {
        this.showPage();
        this.getInitialData();
        clearInterval(loader);
      }
    });
  }

  monthToComparableNumber(date): any {
    if (date === undefined || date === null || date.length !== 10) {
      return null;
    }
    const yearNumber = date.substring(6, 10);
    const monthNumber = date.substring(0, 2);
    const dayNumber = date.substring(3, 5);
    const result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
    return result;
  }

  dateComparator(date1, date2) {
    const date1Number = this.monthToComparableNumber(date1);
    const date2Number = this.monthToComparableNumber(date2);
    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }
    return date1Number - date2Number;
  }

  showPage() {
    document.getElementById('loading').style.display = 'none';
    document.getElementById('offers-container').style.display = 'block';
  }

  getInitialData() {
    this.rowData = this.loggedInUser.receivedOffer;
  }

  markAsReaded() {
    this.selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = this.selectedNodes.map( node => node.data );
    const data = {
      userId: this.loggedInUser._id,
      messageId: selectedData[0]._id,
    };
    this.authService.markAsRead(data)
      .then((res) => {
        this.loggedInUser = res;
        this.rowData = this.loggedInUser.receivedOffer;
        this.agGrid.api.refreshCells();
      });
  }

  goBack () {
    window.history.back();
  }

  editRow() {
    this.selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = this.selectedNodes.map( node => node.data );
    const myArr = [];
    if (selectedData) {
      for (const x of Object.keys(selectedData)) {
        myArr.push( selectedData[x]._id );
     }
    }
    this.authService.me()
      .then(() => {
        this.loggedInUser = this.authService.getUser();
        const result = this.search(selectedData[0]._id, this.loggedInUser);

        const data = {
        userId: this.loggedInUser._id,
        messageId: selectedData[0]._id,
        offerEdited: {
          offerId: selectedData[0].offerId,
          firstName: selectedData[0].firstName,
          surName: selectedData[0].surName,
          phone: selectedData[0].phone,
          email: selectedData[0].email,
          companyName: selectedData[0].companyName,
          address: selectedData[0].address,
          zipCode: selectedData[0].zipCode,
          city: selectedData[0].city,
          country: selectedData[0].country,
          profession: selectedData[0].profession,
          otherProfession: selectedData[0].otherProfession,
          application: selectedData[0].application,
          otherApplication: selectedData[0].otherApplication,
          categoryType: selectedData[0].categoryType,
          modelId: selectedData[0].modelId,
          width: selectedData[0].width,
          height: selectedData[0].height,
          projectName: selectedData[0].projectName,
          messageContent: selectedData[0].messageContent,
          requestStatus: selectedData[0].requestStatus,
          internalComments: selectedData[0].internalComments,
          initialComment: result,
        }
      };
      this.authService.edit(data)
      .then((res) => {
        this.loggedInUser = res;
        this.rowData = this.loggedInUser.receivedOffer;
        this.agGrid.api.refreshCells();
      });
    });
  }

    sizeToFit() {
      this.gridApi.sizeColumnsToFit();
  }

  search( a, b ) {
    for (let i = 0; i < b.receivedOffer.length; i++) {
      if (b.receivedOffer[i]._id === a) {
        return b.receivedOffer[i].internalComments;
      }
    }
  }


  autoSizeAll() {
    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.rowData = this.loggedInUser.receivedOffer;
    this.autoSizeAll();
  }

  showAll() {
    if (this.showStatus === true ) {
      this.showStatus = false;
    } else {
      this.showStatus = true;
    }
    this.gridColumnApi.setColumnsVisible([
      'companyName',
      'address',
      'zipCode',
      'profession',
      'otherProfession',
      'projectName',
      'otherApplication',
    ], this.showStatus);
  }
}
