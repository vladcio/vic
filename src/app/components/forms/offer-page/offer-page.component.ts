import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { ContactService } from '../../../services/contact/contact.service';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-offer-page',
  templateUrl: './offer-page.component.html',
  styleUrls: ['./offer-page.component.css']
})
export class OfferPageComponent implements OnInit {
  user: any;
  error = null;
  processing = false;
  formInvalid = false;
  message: string;
  idForMessages = environment.idForMessage;
  userMessageNumber: any;
  res: any;
  item: any;

  newOfferForm: FormGroup;

  professions = [
    {name: 'Arhitect'},
    {name: 'Designer'},
    {name: 'Metal Construction'},
    {name: 'Facade Sector'},
    {name: 'Carpenter'},
    {name: 'Other Construction Company'},
    {name: 'Private House Owner'},
    {name: 'Inversor'},
    {name: 'Other'},
  ];

  categoryTypes = [
    {name: 'Category 1'},
    {name: 'Category 2'},
    {name: 'Category 3'},
    {name: 'Category 4'},
    {name: 'Category 5'},
    {name: 'Custom'},
  ];

  applications = [
    {name: 'Perforated Facade'},
    {name: 'Balcony Cladding'},
    {name: 'Sliding Screen'},
    {name: 'Balcony Acoustics'},
    {name: 'Stair Railing'},
    {name: 'Wall Cladding'},
    {name: 'Partition Wall'},
    {name: 'Celling Panel'},
    {name: 'Other'},
  ];

  constructor(
     private formBuilder: FormBuilder,
     private authService: AuthService,
     private contactService: ContactService,
     private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    // check user
    this.authService.userChange$.subscribe((user) => {
     this.user = user;
    });
    this.getParams();
    this.authService.me();
    this.createForm();
    this.formControlValueChanged();
    this.formInvalid = false;
    this.getUserIdMessage(this.idForMessages);
  }

  getParams() {
    this.activatedRoute.queryParams
      .subscribe(params => {
        this.item = JSON.parse(params.item);
      });
  }

  createForm() {
    this.newOfferForm = this.formBuilder.group({
      firstName: ['', Validators.required ],
      surName: ['', Validators.required ],
      phone: ['', Validators.pattern(/^[0-9, +]{5,15}$/)],
      email: ['', [Validators.required, CustomValidators.email]],
      companyName: '',
      address: ['', Validators.required ],
      zipCode: ['', Validators.required ],
      city: ['', Validators.required ],
      country: ['Spain', Validators.required ],
      profession: '',
      otherProfession: '',
      application: ['', Validators.required ],
      otherApplication: '',
      categoryType: [this.item ? this.item.categoryType : '', Validators.required ],
      modelId: this.item ? this.item.idReference : '',
      width: ['0.00', Validators.pattern(/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/)],
      height: ['0.00', Validators.pattern(/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/)],
      projectName: '',
      messageContent: '',
    });
  }

  formControlValueChanged() {
    const modelId = this.newOfferForm.get('modelId');
    const otherApplication = this.newOfferForm.get('otherApplication');
    this.newOfferForm.get('categoryType').valueChanges.subscribe(
      (categoryType: string) => {
        if (categoryType !== 'Custom') {
            modelId.setValidators([Validators.required]);
        } else {
            modelId.clearValidators();
        }
        modelId.updateValueAndValidity();
    });
    this.newOfferForm.get('application').valueChanges.subscribe(
      (application: string) => {
        if (application === 'Other') {
            otherApplication.setValidators([Validators.required]);
        } else {
            otherApplication.clearValidators();
        }
        otherApplication.updateValueAndValidity();
    });
  }

  get firstName() { return this.newOfferForm.get('firstName'); }
  get surName() { return this.newOfferForm.get('surName'); }
  get phone() { return this.newOfferForm.get('phone'); }
  get email() { return this.newOfferForm.get('email'); }
  get companyName() { return this.newOfferForm.get('companyName'); }
  get address() { return this.newOfferForm.get('address'); }
  get city() { return this.newOfferForm.get('city'); }
  get zipCode() { return this.newOfferForm.get('zipCode'); }
  get country() { return this.newOfferForm.get('country'); }
  get profession() { return this.newOfferForm.get('profession'); }
  get otherProfession() { return this.newOfferForm.get('otherProfession'); }
  get application() { return this.newOfferForm.get('application'); }
  get otherApplication() { return this.newOfferForm.get('otherApplication'); }
  get categoryType() { return this.newOfferForm.get('categoryType'); }
  get modelId() { return this.newOfferForm.get('modelId'); }
  get width() { return this.newOfferForm.get('width'); }
  get height() { return this.newOfferForm.get('height'); }
  get projectName() { return this.newOfferForm.get('projectName'); }
  get messageContent() { return this.newOfferForm.get('messageContent'); }


  getUserIdMessage(id) {
    this.authService.getUserIdMessage(id)
      .then((res) => {
        this.userMessageNumber = res;
      });
  }

  onSubmit(offer) {
    this.processing = false;
    this.formInvalid = false;
    this.error = '';
    this.message = '';
    const formData = new FormData();
    const data = {
      firstName: offer.value.firstName,
      surName: offer.value.surName,
      phone: offer.value.phone,
      email: offer.value.email,
      companyName: offer.value.companyName,
      address: offer.value.address,
      zipCode: offer.value.zipCode,
      country: offer.value.country,
      profession: offer.value.profession,
      otherProfession: offer.value.otherProfession,
      application: offer.value.application,
      otherApplication: offer.value.otherApplication,
      categoryType: offer.value.categoryType,
      modelId: offer.value.modelId,
      width: offer.value.width,
      height: offer.value.height,
      projectName: offer.value.projectName,
      messageContent: offer.value.messageContent,
      id: this.idForMessages,
      offerId: this.userMessageNumber,
    };

    if (this.newOfferForm.valid) {
      this.contactService.sendOfferForm(data)
        .then((result) => {
         this.res = result;
        })
        .catch((err) => {
          this.error = err.error.error;
          this.processing = false;
        });
        this.processing = true;
        this.message = 'Your request has been sent';
    } else if (this.newOfferForm.invalid) {
      this.formInvalid = true;
    }
    this.rebuildForm();
    setTimeout( () => {
      this.getUserIdMessage(this.idForMessages);
      this.processing = false;
    }, 5000);

  }

  rebuildForm() {
    if (this.newOfferForm.valid) {
      this.newOfferForm.reset();
      this.formInvalid = false;
    }
  }
  goBack() {
    window.history.back();
  }

}
