import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControlOptions, AsyncValidatorFn } from '@angular/forms';
import { AuthService } from '../../../services/auth/auth.service';
import { ContactService } from '../../../services/contact/contact.service';
import { environment } from '../../../../environments/environment';
import { CustomValidators } from 'ng2-validation';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.css']
})
export class ContactPageComponent implements OnInit {
  user: any;
  error = null;
  processing = false;
  formInvalid = false;
  message: string;
  idForMessages = environment.idForMessage;
  res: any;

  newContactForm: FormGroup;
  reasons = [
    {name: 'Details about products'},
    {name: ''},
    {name: '3'},
  ];
  defaultReason = '1';
  // emailRegEx = /^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$/;

  constructor(
     private formBuilder: FormBuilder,
     private authService: AuthService,
     private contactService: ContactService,
  ) {

    this.createForm();
  }

  createForm() {
    this.newContactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, CustomValidators.email]],
      phoneNumber: ['', Validators.pattern(/^[0-9, +]{5,15}$/)],
      messageContent: ['', Validators.required],
    });
  }

  get name() { return this.newContactForm.get('name'); }
  get email() { return this.newContactForm.get('email'); }
  get phoneNumber() { return this.newContactForm.get('phoneNumber'); }
  get messageContent() { return this.newContactForm.get('messageContent'); }

   ngOnInit() {
     // check user
     this.authService.userChange$.subscribe((user) => {
       this.user = user;
      });
      this.authService.me();
      this.formInvalid = false;
    }

  onSubmit(contact) {
    this.processing = false;
    this.formInvalid = false;
    this.error = '';
    this.message = '';
    const formData = new FormData();
    const data = {
      name: contact.value.name,
      email: contact.value.email,
      phoneNumber: contact.value.phoneNumber,
      messageContent: contact.value.messageContent,
      id: this.idForMessages,
    };

    if (this.newContactForm.valid) {
      this.contactService.sendContactForm(data)
      .then((res) => {
        this.res = res;
      })
        .catch((err) => {
          this.error = err.error;
          this.processing = false;
        });
        this.processing = true;
        this.message = 'Your message has been sent';

    } else if (this.newContactForm.invalid) {
      this.formInvalid = true;
    }
    this.rebuildForm();
    setTimeout( () => {
      this.processing = false;
    }, 5000);

  }

  rebuildForm() {
    if (this.newContactForm.valid) {
      this.newContactForm.reset();
      this.formInvalid = false;
    }
  }
  goBack() {
    window.history.back();
  }

}
