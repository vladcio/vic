import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProviderAst, } from '@angular/compiler';
import { CategoriesService } from '../../../services/categories/categories.service';
import { AuthService } from '../../../services/auth/auth.service';
import { PicturesService } from '../../../services/pictures/pictures.service';


@Component({
  selector: 'app-new-picture',
  templateUrl: './new-picture.component.html',
  styleUrls: ['./new-picture.component.css']
})
export class NewPictureComponent implements OnInit {

  @ViewChild('fileInput')
  fileInput: ElementRef;
  user: any;
  error = null;
  processing = false;
  formInvalid = false;
  message: string;
  fileName: string;
  uploadedFile: any;

  newPictureForm: FormGroup;
  types = [
    {name: 'interior'},
    {name: 'exterior'},
  ];

  modelTypes = [
    {name: '1'},
    {name: '2'},
    {name: '3'},
    {name: '4'},
    {name: '5'},
    {name: 'Custom'},
  ];

  constructor(
     private formBuilder: FormBuilder,
     private pictureService: PicturesService,
     private authService: AuthService,
     private cd: ChangeDetectorRef,
     private categoriesService: CategoriesService,
  ) {

    this.createForm();
   }

   createForm() {
     this.newPictureForm = this.formBuilder.group({
      name: ['', Validators.required ],
      categoryType: ['', Validators.required ],
      modelType: ['', Validators.required ],
      picPath: this.fileName,
      description: '',
      idReference: ['', Validators.required ],
     });
   }

   get name() { return this.newPictureForm.get('name'); }
   get categoryType() { return this.newPictureForm.get('categoryType'); }
   get modelType() { return this.newPictureForm.get('modelType'); }
   get picPath() { return this.newPictureForm.get('picPath'); }
   get getDescription() { return this.newPictureForm.get('description'); }
   get idReference() { return this.newPictureForm.get('idReference'); }

  ngOnInit() {
    // check user
    this.authService.userChange$.subscribe((user) => {
      this.user = user;
    });
    this.authService.me();
    this.formInvalid = false;
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const inputEl: any = document.querySelector('#file-input');
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.newPictureForm.patchValue({
          file: reader.result
       });
       this.cd.markForCheck();
       this.newPictureForm.controls['picPath'].setValue(file ? file.name : '');
       if (inputEl.files && inputEl.files[0]) {
         const formData = new FormData();
         formData.append('uploadedFile', inputEl.files[0]);
         this.categoriesService.addPhotoItem(formData)
           .then((result) => {
            this.uploadedFile = result;
           });
       }
      };
    }
  }

  onSubmit(picture) {
    this.processing = false;
    this.formInvalid = false;
    this.error = '';
    this.message = '';
    const data = {
      name: picture.value.name,
      categoryType: picture.value.categoryType,
      modelType: picture.value.modelType,
      picPath: this.uploadedFile.filename,
      description: picture.value.description,
      idReference: picture.value.idReference,
    };
    const idReference = picture.value.idReference;
    if (this.newPictureForm.valid) {
      this.pictureService.addPicture(data)
        .then((result) => {
        })
        .catch((err) => {
          this.error = err.error.error;
          this.processing = false;
        });
        this.processing = true;
        this.message = 'Id ' + picture.value.idReference + ' - Succesfully added.';

      } else if (this.newPictureForm.invalid) {
        this.formInvalid = true;
      }
      this.rebuildForm();
      setTimeout( () => {
        this.processing = false;
      }, 5000);

    }

  rebuildForm() {
    if (this.newPictureForm.valid) {
      this.newPictureForm.reset();
      this.fileInput.nativeElement.value = '';
      this.formInvalid = false;
    }
  }

}
