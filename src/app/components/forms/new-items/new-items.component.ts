import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProviderAst, } from '@angular/compiler';
import { CategoriesService } from '../../../services/categories/categories.service';
import { AuthService } from '../../../services/auth/auth.service';


@Component({
  selector: 'app-new-items',
  templateUrl: './new-items.component.html',
  styleUrls: ['./new-items.component.css']
})
export class NewItemsComponent implements OnInit {

  @ViewChild('fileInput')
  fileInput: ElementRef;
  user: any;
  error = null;
  processing = false;
  formInvalid = false;
  message: string;
  fileName: string;
  uploadedFile: any;

  newItemForm: FormGroup;
  types = [
    {name: 'Category 1'},
    {name: 'Category 2'},
    {name: 'Category 3'},
    {name: 'Category 4'},
    {name: 'Category 5'},
    {name: 'Custom'},
  ];

  constructor(
     private formBuilder: FormBuilder,
     private categoriesService: CategoriesService,
     private authService: AuthService,
     private cd: ChangeDetectorRef,
  ) {

    this.createForm();
   }

   createForm() {
     this.newItemForm = this.formBuilder.group({
      name: ['', Validators.required ],
      categoryType: ['', Validators.required ],
      picPath: this.fileName,
      description: '',
      idReference: ['', Validators.required ],
     });
   }

   get name() { return this.newItemForm.get('name'); }
   get categoryType() { return this.newItemForm.get('categoryType'); }
   get picPath() { return this.newItemForm.get('picPath'); }
   get getDescription() { return this.newItemForm.get('description'); }
   get idReference() { return this.newItemForm.get('idReference'); }

  ngOnInit() {
    // check user
    this.authService.userChange$.subscribe((user) => {
      this.user = user;
    });
    this.authService.me();
    this.formInvalid = false;
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const inputEl: any = document.querySelector('#fileInput');
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.newItemForm.patchValue({
          file: reader.result
       });
      //  this.uploadedFile = file.name;
       // need to run CD since file load runs outside of zone
       this.cd.markForCheck();
       this.newItemForm.controls['picPath'].setValue(file ? file.name : '');
       if (inputEl.files && inputEl.files[0]) {
         const formData = new FormData();
         formData.append('uploadedFile', inputEl.files[0]);
         this.categoriesService.addPhotoItem(formData)
           .then((result) => {
            this.uploadedFile = result;
           });
       }
      };
    }
  }

  onSubmit(item) {
    this.processing = false;
    this.formInvalid = false;
    this.error = '';
    this.message = '';
    const formData = new FormData();
    const id = item.value.idReference;
    const data = {
      name: item.value.name,
      categoryType: item.value.categoryType,
      picPath: this.uploadedFile.filename,
      description: item.value.description,
      idReference: item.value.idReference,
    };

    if (this.newItemForm.valid) {
      this.categoriesService.addItem(data)
        .then((result) => {
        })
        .catch((err) => {
          this.error = err.error.error;
          this.processing = false;
        });
        this.processing = true;
        this.message = 'Id ' + id + ' - Succesfully added.';

    } else if (this.newItemForm.invalid) {
      this.formInvalid = true;
    }
    this.rebuildForm();
    setTimeout( () => {
      this.processing = false;
    }, 5000);

  }

  rebuildForm() {
    if (this.newItemForm.valid) {
      this.newItemForm.reset();
      this.fileInput.nativeElement.value = '';
      this.formInvalid = false;
    }
  }

}
