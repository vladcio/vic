import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';

const API_URL = environment.apiUrl;

@Component({
  selector: 'app-item-model',
  templateUrl: './item-model.component.html',
  styleUrls: ['./item-model.component.css']
})
export class ItemModelComponent implements OnInit {

  @Input() item: any;
  pictureLink: string;

  constructor() { }

  ngOnInit() {
    // establish picture pictureLink after upload
    this.pictureLink = API_URL + this.item.picPath;
  }
}
