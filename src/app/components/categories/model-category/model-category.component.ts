import { Component, OnInit, Input, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-model-category',
  templateUrl: './model-category.component.html',
  styleUrls: ['./model-category.component.css']
})
export class ModelCategoryComponent implements OnInit {

@Input() items: any;
item: any;

  constructor() { }

  ngOnInit() {

  const loader = setInterval(() => {
      if (this.items) {
        this.showPage();
        clearInterval(loader);
      }
    });
  }

  showPage() {
    document.getElementById('loading').style.display = 'none';
    document.getElementById('list-all-items').style.display = 'block';
  }

  goBack () {
    window.history.back();
  }

}
