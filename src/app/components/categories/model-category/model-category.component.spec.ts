import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelCategoryComponent } from './model-category.component';

describe('ModelCategoryComponent', () => {
  let component: ModelCategoryComponent;
  let fixture: ComponentFixture<ModelCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
