import { Component, OnInit, Input } from '@angular/core';
import { CategoriesService } from '../../../services/categories/categories.service';

@Component({
  selector: 'app-category4',
  templateUrl: '../model-category/model-category.component.html',
  styleUrls: ['../model-category/model-category.component.css']
})
export class Category4Component implements OnInit {

  data: any;
  items: Array<any>;
  categoryType = '4';

  constructor(
    private categories: CategoriesService,
  ) { }

  ngOnInit() {

    this.data = {
      categoryType: this.categoryType
    };
    this.categories.getCategory(this.data)
      .then((items) => {
      this.items = items;
      this.showPage();
    });
  }

  showPage() {
    document.getElementById('loading').style.display = 'none';
    document.getElementById('list-all-items').style.display = 'block';
  }

  goBack () {
    window.history.back();
  }
}
