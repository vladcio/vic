import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';


import { AppComponent } from './app.component';
import { VICDHomeComponent } from './pages/vicd-home/vicd-home.component';
import { ModelsPageComponent } from './pages/models-page/models-page.component';
import { GalleryPageComponent } from './pages/gallery-page/gallery-page.component';
import { PagesMenuComponent } from './components/menus/pages-menu/pages-menu.component';
import { Category1Component } from './components/categories/category1/category1.component';
import { Category2Component } from './components/categories/category2/category2.component';
import { Category3Component } from './components/categories/category3/category3.component';
import { Category4Component } from './components/categories/category4/category4.component';
import { Category5Component } from './components/categories/category5/category5.component';
import { InteriorComponent } from './components/gallery/interior/interior.component';
import { ExteriorComponent } from './components/gallery/exterior/exterior.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { OfferPageComponent } from './components/forms/offer-page/offer-page.component';
import { ContactPageComponent } from './components/forms/contact-page/contact-page.component';
import { LoginComponent } from './pages/login/login.component';
import { CustomComponent } from './components/categories/custom/custom.component';
import { NewItemsComponent } from './components/forms/new-items/new-items.component';
import { ModelCategoryComponent } from './components/categories/model-category/model-category.component';
import { NewPictureComponent } from './components/forms/new-picture/new-picture.component';
import { CategoriesService } from './services/categories/categories.service';
import { AuthService } from './services/auth/auth.service';
import { PicturesService } from './services/pictures/pictures.service';
import { ModelPictureCategoryComponent } from './components/gallery/model-picture-category/model-picture-category.component';
import { ItemModelComponent } from './components/categories/item-model/item-model.component';
import { PictureModelComponent } from './components/gallery/picture-model/picture-model.component';
import { UsersComponent } from './pages/users/users.component';
import { MessagesComponent } from './components/admin/messages/messages.component';
import { OffersComponent } from './components/admin/offers/offers.component';
import { RequireUserGuardServiceService } from './services/guard/require-user-guard-service.service';

const routes: Routes = [
  {path: '', component: VICDHomeComponent},
  {path: 'models', component: ModelsPageComponent},
  {path: 'models/cat1', component: Category1Component},
  {path: 'models/cat2', component: Category2Component},
  {path: 'models/cat3', component: Category3Component},
  {path: 'models/cat4', component: Category4Component},
  {path: 'models/cat5', component: Category5Component},
  {path: 'models/custom', component: CustomComponent},
  {path: 'gallery', component: GalleryPageComponent},
  {path: 'gallery/interior', component: InteriorComponent},
  {path: 'gallery/exterior', component: ExteriorComponent},
  {path: 'about', component: AboutPageComponent},
  {path: 'offer', component: OfferPageComponent},
  {path: 'offer/:id', component: OfferPageComponent},
  {path: 'contact', component: ContactPageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'new-item', component: NewItemsComponent, canActivate: [RequireUserGuardServiceService]},
  {path: 'new-picture', component: NewPictureComponent, canActivate: [RequireUserGuardServiceService]},
  {path: 'users', component: UsersComponent, canActivate: [RequireUserGuardServiceService]},
  {path: 'admin/messages', component: MessagesComponent, canActivate: [RequireUserGuardServiceService]},
  {path: 'admin/offers', component: OffersComponent, canActivate: [RequireUserGuardServiceService]},
];



@NgModule({
  declarations: [
    AppComponent,
    VICDHomeComponent,
    ModelsPageComponent,
    GalleryPageComponent,
    PagesMenuComponent,
    Category1Component,
    Category2Component,
    Category3Component,
    Category4Component,
    Category5Component,
    InteriorComponent,
    ExteriorComponent,
    AboutPageComponent,
    OfferPageComponent,
    ContactPageComponent,
    LoginComponent,
    CustomComponent,
    NewItemsComponent,
    ModelCategoryComponent,
    NewPictureComponent,
    ModelPictureCategoryComponent,
    ItemModelComponent,
    PictureModelComponent,
    UsersComponent,
    MessagesComponent,
    OffersComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([]),
  ],
  providers: [AuthService, CategoriesService, PicturesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
